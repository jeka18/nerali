$(function(){

	/*STEPS*/
	$('.hover').mouseover(function(){
	    var $draw = $(this).find('.draw');
	    $('.draw').removeClass('active');
	    $draw.addClass('active');
	  	
	  	$('.steps-text p').removeClass('active');
	    var $data = $(this).attr('data-target');
	    $($data).addClass('active');
	});

	$('.hover').mouseout(function(){
	    var $text = $(this).find('.text');
	    $text.fadeOut();
	    var $draw = $(this).find('.draw');
	    $draw.removeClass('active');
	  
	    $('.article-item').removeClass('active');
	});


	/*PLAN MORE*/

	$(document).mouseup(function (e){ 
		var div = $(".plan-list");
		if (!div.is(e.target) 
		    && div.has(e.target).length === 0) { 
			$(div).removeClass('active');
			$('.plan-more').removeClass('active');
		}
	});

	var plan1 = false;
	var plan2 = false;
	var plan3 = false;

	$('.plan1').click(function(e){
		e.preventDefault();

		if(plan1 == false){
			$(this).addClass('active');
			$('.plan2, .plan3').removeClass('active');
			$('.plan-list').addClass('active');
			plan1 = true;
			plan2 = false;
			plan3 = false;
			if($('.plan1').hasClass('active')){
				$('.step1').addClass('hide');
				$('.step1, .step2, .step3').removeClass('show');
			}

		} else{
			$(this).removeClass('active');
			$('.plan-list').removeClass('active');
			plan1 = false;
		}

		if(plan1 == true && $('.plan-list').is(':visible')){
			plan1 = true
			

		}

	});

	$('.plan2').click(function(e){
		e.preventDefault();

		if(plan2 == false){
			$(this).addClass('active');
			$('.plan1, .plan3').removeClass('active');
			$('.plan-list').addClass('active');
			plan2 = true;
			plan1 = false;
			plan3 = false;
			if($('.plan2').hasClass('active')){
				$('.step2').addClass('show');
				$('.step3').removeClass('show');
			}

		} else{
			$(this).removeClass('active');
			$('.plan-list').removeClass('active');
			plan2 = false;
		}
	
	});

	$('.plan3').click(function(e){
		e.preventDefault();

		if(plan3 == false){
			$(this).addClass('active');
			$('.plan2, .plan1').removeClass('active');
			$('.plan-list').addClass('active');
			plan3 = true;
			plan1 = false;
			plan2 = false;
			if($('.plan3').hasClass('active')){
				$('.step3, .step2').addClass('show');
			}

		} else{
			$(this).removeClass('active');
			$('.plan-list').removeClass('active');
			plan3 = false;
		}

		
	});


	/*FORM VALIDATION*/

	var check = false;
	$('.modal-form form').on('change keyup click',function(){
		
		if($('#check').is(':checked')){
			check = true;
		}

		if(check == false){
			$('.submit').attr('disabled', 'disabled');
			check = true;
		} else{
			$('.submit').removeAttr('disabled');
			check = false;
		}
	});

	

	


	/*MODAL*/
	$('.open-form').click(function(e){
    e.preventDefault();
    $('.overflow').fadeIn();
     
  });
 

  $(document).mouseup(function (e){ 
		var div = $(".modal-form");
		if (!div.is(e.target) 
		    && div.has(e.target).length === 0) { 
			$('.overflow').fadeOut();
		}
	});

});

